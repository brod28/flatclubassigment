﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Framework.Common.Base
{
    public class Base
    {
        protected ILoggerService _logger = ObjectFactory.GetInstance<ILoggerService>();
        protected ICacheManager _cache = ObjectFactory.GetInstance<ICacheManager>();
        protected IConfiguration _config = ObjectFactory.GetInstance<IConfiguration>();
                     
    }
}
