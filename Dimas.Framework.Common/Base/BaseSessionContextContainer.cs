﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;

namespace Dimas.Framework.Common.Base
{
    public class FakeBaseSessionContextContainer : IBaseSessionContextContainer
    {
        public string userName
        {
            get 
            { 
                return string.Empty; 
            }
        }

        public Guid SessionTransactionId
        {
            get 
            { 
                return new Guid(); 
            }
        }
    }
}
