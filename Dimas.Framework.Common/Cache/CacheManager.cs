﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;
using Dimas.Framework.Interface;
namespace Dimas.Framework.Common.Cache
{
    public class CacheManager : ICacheManager
    {
        private static ObjectCache cache = MemoryCache.Default;

        public bool Add(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return cache.Add(key, value, absoluteExpiration);
        }

        public object Get(string key)
        {
            return cache.Get(key);           
        }
        public bool Contains(string key)
        {
            return cache.Contains(key);
        }

        
    }
}
