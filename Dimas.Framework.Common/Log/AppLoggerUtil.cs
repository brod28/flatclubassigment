﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dimas.Framework.Interface;

namespace Dimas.Framework.Common.Log
{
    public class AppLoggerUtil
    {
        public static string ParseSessionContextContainer(object scc)
        {
            return scc.ToString();
        }

        public static string ParseExceptionForLog(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            while (ex != null)
            {
                sb.AppendLine(ex.Message);
                sb.Append(ex.StackTrace);
                sb.AppendLine("------------");
                ex = ex.InnerException;
            }
            return sb.ToString();
        }

        public static string ParseStringFormessage(string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(message);
            return sb.ToString();
        }
    }
}
