﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using Dimas.Framework.Interface;

namespace Dimas.Framework.Common.Log
{
    public class Log4NetConfig : ILogConfig
    {
        public void Start()
        {
            log4net.GlobalContext.Properties["ProcName"] = Process.GetCurrentProcess().ProcessName;

            log4net.Config.XmlConfigurator.Configure();
        }
    }
}