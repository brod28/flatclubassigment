﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using log4net.Core;
using System.Reflection;
using log4net.Appender;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Framework.Common.Log
{
    public class Log4NetLoggerService : ILoggerService
    {
        private ILog _logger;
        private IBaseSessionContextContainer _sessionContextContainer;

        public Log4NetLoggerService()
        {
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
           _sessionContextContainer = ObjectFactory.TryGetInstance<IBaseSessionContextContainer>();
           log4net.GlobalContext.Properties["SessionTransactionId"] = _sessionContextContainer.SessionTransactionId;
           log4net.GlobalContext.Properties["userName"] = _sessionContextContainer.userName;

        }

       
        public void Info(string message)
        {
            message=AppLoggerUtil.ParseStringFormessage(message);
            _logger.Info(message);
        }
        
        public void Warn(string message)
        {
            message = AppLoggerUtil.ParseStringFormessage(message);
            _logger.Warn(message);
        }

        public void Debug(string message)
        {
            message = AppLoggerUtil.ParseStringFormessage(message);
            _logger.Debug(message);
        }

        public void Error(string message)
        {
            message = AppLoggerUtil.ParseStringFormessage(message);
            _logger.Error(message);
        }

        public void Error(Exception ex)
        {
            string message = AppLoggerUtil.ParseExceptionForLog(ex);
            _logger.Error(message);
        }

        public void Fatal(string message)
        {
            message = AppLoggerUtil.ParseStringFormessage(message);
            _logger.Fatal(message);
        }

        public void Fatal(Exception ex)
        {
            string message = AppLoggerUtil.ParseExceptionForLog(ex);
            _logger.Fatal(message);
        }


    }
}
