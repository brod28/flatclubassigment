﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Framework.Common.Utils
{
    public class WebConfiguration : IConfiguration
    {
        public string GetConnectionStrings(string connectionStringName)
        {
            var retVal = string.Empty;
            try
            {
                retVal = System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
                if (string.IsNullOrEmpty(retVal))
                {
                    var _logger = ObjectFactory.GetInstance<ILoggerService>();
                    var message = string.Format("connection string with name is not exist {0}", connectionStringName);
                    _logger.Error(message);
                }
            }
            catch (Exception ex) 
            {
                var _logger = ObjectFactory.GetInstance<ILoggerService>();
                _logger.Error(ex);
            }
            return retVal;
        }
    }
}
