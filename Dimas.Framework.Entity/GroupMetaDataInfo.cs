﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dimas.Framework.Entity
{
    public class GroupMetaDataInfo   
    {
        public GroupInfo Group { get; set; }
        public int SumOfUser { get; set; }
        public int SumOfStories { get; set; }
    }
}
