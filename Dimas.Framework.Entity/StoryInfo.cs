﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Dimas.Framework.Entity
{
    public class StoryInfo : BaseTemplate
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime PostedOn { get; set; }
        
        [Required]
        public int UserRefId { get; set; }
        [ForeignKey("UserRefId")]
        public UserInfo User { get; set; }

        [Required]
        public int GroupRefId { get; set; }
        [ForeignKey("GroupRefId")]
        public GroupInfo Group { get; set; }
    }
}
