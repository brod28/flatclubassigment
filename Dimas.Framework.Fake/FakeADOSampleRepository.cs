﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;

namespace Dimas.Framework.Fake.Repostory
{
    public class FakeADOSampleRepository : IADOSampleRepository
    {
        public SampleInfo GetById(int Id)
        {
            return new SampleInfo() { Id = 2, Name = "natasha" };
        }
    }
}
