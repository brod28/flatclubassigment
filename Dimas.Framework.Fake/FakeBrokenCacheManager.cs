﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;

namespace Dimas.Framework.Fake
{
    public class FakeBrokenCacheManager : ICacheManager
    {
        public bool Add(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return false;
        }

        public object Get(string key)
        {
            return null;
        }

        public bool Contains(string key)
        {
            return false;
        }
    }
}
