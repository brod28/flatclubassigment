﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;

namespace Dimas.Framework.Fake
{
    public class FakeCacheManager : ICacheManager
    {
        static Dictionary<string, object> dic = new Dictionary<string, object>(); 
        public bool Add(string key, object value, DateTimeOffset absoluteExpiration)
        {
            if (dic.ContainsKey(key))
            {
                dic.Remove(key);
            }
            dic.Add(key, value);
            return true;
        }

        public object Get(string key)
        {
            return dic[key];
        }

        public bool Contains(string key)
        {
            return dic.ContainsKey(key);
        }
    }
}
