﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;

namespace Dimas.Framework.Fake.Repostory
{
    public class FakeEarlyBindingSampleRepository : IEarlyBindingSampleRepository
    {
        public SampleInfo GetById(int id)
        {
            return new SampleInfo() { Id = 1, Name = "dima" };
        }
    }
}
