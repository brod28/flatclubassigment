﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;

namespace Dimas.Framework.Fake.Repostory
{
    public class FakeEntityFrameworkSampleRepository : IEntityFrameworkSampleRepository
    {
        ResponseSampleService IEntityFrameworkSampleRepository.GetById(int Id)
        {
            return new ResponseSampleService() { Id = 1, Name = "dima" };
        }
    }
}
