﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;

namespace Dimas.Framework.Fake
{
    public class FakeLoggerService : ILoggerService
    {
        public void Info(string message)
        {
        }

        public void Warn(string message)
        {
        }

        public void Debug(string message)
        {
        }

        public void Error(string message)
        {
        }

        public void Error(Exception ex)
        {
        }

        public void Fatal(string message)
        {
        }

        public void Fatal(Exception ex)
        {
        }
    }
}
