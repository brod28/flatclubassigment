﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimas.Framework.Interface
{
    public interface IBaseSessionContextContainer
    {
        string userName { get; }
        Guid SessionTransactionId { get;}
    }
}
