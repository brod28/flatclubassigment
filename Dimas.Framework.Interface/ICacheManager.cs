﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimas.Framework.Interface
{
    public interface ICacheManager
    {
        bool Add(string key, object value, DateTimeOffset absoluteExpiration);
        object Get(string key);
        bool Contains(string key);
    }
}
