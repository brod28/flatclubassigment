﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dimas.Framework.Interface
{
    public interface IConfiguration
    {
        string GetConnectionStrings(string connectionStringName);
    }
}
