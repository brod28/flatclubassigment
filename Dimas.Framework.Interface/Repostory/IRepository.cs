﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Dimas.Framework.Entity;
namespace Dimas.Framework.Interface.Repostory
{
    public interface IRepository<T> : IDisposable where T:BaseTemplate
    {
        IDbSet<T> Items { get; }
        int SaveChanges();
        
    }
}
