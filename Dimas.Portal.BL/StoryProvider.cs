﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;

namespace Dimas.Portal.BL
{
    public class StoryProvider
    {
        public List<GroupInfo> GetGroups()
        {
            List<GroupInfo> retVal = null;
            using (var groupRepository = StructureMap.ObjectFactory.GetInstance<IRepository<GroupInfo>>())
            {
                retVal = (from i in groupRepository.Items
                              select i)
                           .ToList();

            }
            return retVal;
        }

        public List<GroupMetaDataInfo> GroupDetails()
        {
            List<GroupMetaDataInfo> retVal = null;
            using (var storyRepository = StructureMap.ObjectFactory.GetInstance<IRepository<StoryInfo>>())
            {
                // select just the stories from relevant group by name
                var storyByGroupName = from i in storyRepository.Items
                                       select i;

                // group stories by user , group and sum of them
                var storyGroupedByUserAndGroup = from i in storyByGroupName
                                                 group i by new
                                                 {
                                                     UserRefId = i.UserRefId,
                                                     GroupRefId = i.GroupRefId
                                                 } into g

                                                 select new
                                                 {
                                                     UserRefId = g.Key.UserRefId,
                                                     GroupRefId = g.Key.GroupRefId,
                                                     User = g.FirstOrDefault().User,
                                                     Group = g.FirstOrDefault().Group,
                                                     SumOfStories = g.Count()
                                                 };

                // group stories and users by group and sum number of users in the group and sum number of stories by the group 
                var storyGroupedByGroup = from i in storyGroupedByUserAndGroup
                                          group i by new
                                          {
                                              GroupRefId = i.GroupRefId
                                          } into g

                                          select new GroupMetaDataInfo()
                                          {
                                              Group = g.FirstOrDefault().Group,
                                              SumOfUser = g.Count(),
                                              SumOfStories = g.Sum(a => a.SumOfStories)
                                          };
                retVal = storyGroupedByGroup.ToList();
            }
            return retVal;
        
}

        public StoryInfo CreateStory(StoryInfo entity)
        {
            using (var storyRepository = StructureMap.ObjectFactory.GetInstance<IRepository<StoryInfo>>())
            {
                entity=storyRepository.Items.Add(entity);
                storyRepository.SaveChanges();
            }
            return entity;
        }

        public List<StoryInfo> GetStoriesByUserId(int userId)
        {
            List<StoryInfo> retVal = null;
            using (var storyRepository = StructureMap.ObjectFactory.GetInstance<IRepository<StoryInfo>>())
            {
                retVal = (from i in storyRepository.Items
                              where i.UserRefId ==userId 
                             select i)
                             .ToList();

            }
            return retVal;
        }
    }
}
