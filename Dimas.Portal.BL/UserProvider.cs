﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;

namespace Dimas.Portal.BL
{
    public class UserProvider
    {
        public UserInfo GetUserByName(string username)
        {
            UserInfo retVal=null;
            using (var storyRepository = StructureMap.ObjectFactory.GetInstance<IRepository<UserInfo>>())
            {
                    retVal=(from i in storyRepository.Items
                            where i.Name == username
                            select i)
                            .FirstOrDefault();
            }
            return retVal;
        }

        public UserInfo CreateUser(UserInfo user)
        {
            UserInfo retVal=null;
            using (var storyRepository = StructureMap.ObjectFactory.GetInstance<IRepository<UserInfo>>())
            {
                retVal = storyRepository.Items.Add(user);
                    storyRepository.SaveChanges();
            }
           
            return retVal;
        }

        public UserInfo CreateUserIfNotExist(string username)
        {

            UserInfo retVal = GetUserByName(username);

            if (retVal == null)
            {
                retVal = new UserInfo()
                {
                    Name = username
                };
                retVal = CreateUser(retVal);
            }
            return retVal;

        }
    }
}
