﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Entity;

namespace Dimas.Portal.DAL
{
    public class Context : DbContext
    {
        public Context()
            : base("name=flatClub")
        {
        }
        public DbSet<UserInfo> _users { get; set; }
        public DbSet<StoryInfo> _stories { get; set; }
        public DbSet<GroupInfo> _groups { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
