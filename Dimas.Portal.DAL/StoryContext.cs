﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;
using Dimas.Framework.Interface;

namespace Dimas.Portal.DAL
{
   public class StoryContext : IRepository<StoryInfo>, IRepository<GroupInfo>, IRepository<UserInfo>, IDisposable
    {
        private static bool _isInitialized=false;
        private static Object thisLock = new Object();

        private Context _context = new Context();
        public StoryContext() 
        { 
            if(!_isInitialized)
            {
                //lock only if true 
                lock (thisLock)
                {
                    //in case app started and group table is empty
                    if (!_isInitialized && _context._groups.Count()==0)
                    {

                        var item = new GroupInfo()
                        {
                            Name = "Sport",
                            Description = "Sport Games"
                        };
                        _context._groups.Add(item);
                        item = new GroupInfo()
                        {
                            Name = "News",
                            Description = "Newspapers and Blogs"
                        };
                        _context._groups.Add(item);
                        item = new GroupInfo()
                        {
                            Name = "Movie",
                            Description = "Drama  and Comedia"
                        };
                        _context._groups.Add(item);
                        item = new GroupInfo()
                        {
                            Name = "Music",
                            Description = "Pop and classic"
                        };
                        _context._groups.Add(item);
                        item = new GroupInfo()
                        {
                            Name = "Food",
                            Description = "Asian and South America"
                        };
                        _context._groups.Add(item);
                        _context.SaveChanges();
                    }
                    _isInitialized = true;
                }
            }
        }

        IDbSet<StoryInfo> IRepository<StoryInfo>.Items
        {
            get
            {
                return _context._stories;
            }
        }

        IDbSet<GroupInfo> IRepository<GroupInfo>.Items
        {
            get
            {
                return _context._groups;
            }
        }

        IDbSet<UserInfo> IRepository<UserInfo>.Items
        {
            get
            {
                return _context._users;
            }
        }



        public void Dispose()
        {
            _context.Dispose();
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
