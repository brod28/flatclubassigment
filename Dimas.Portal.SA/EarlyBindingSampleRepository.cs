﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Common.Utils;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;
using Dimas.Portal.SA.Proxy;

namespace Dimas.Portal.SA
{
    public class EarlyBindingSampleRepository : IEarlyBindingSampleRepository
    {
        public SampleInfo GetById(int id)
        {
            var url = System.Configuration.ConfigurationManager.AppSettings["SampleServiceUrl"];
            //update proxy url (endpoint url maybe have been changed)
            SampleService proxy = new SampleService() { Url = url };
            var request = new Dimas.Portal.SA.Proxy.RequestSampleService() { IdToSearch = id };
            var response = proxy.GetById(request);
            var result = Mapper.Map<Dimas.Portal.SA.Proxy.ResponseSampleService, SampleInfo>(response);
            return result;
        }
    }
}
