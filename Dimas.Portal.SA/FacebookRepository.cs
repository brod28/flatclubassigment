﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;
using Facebook;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Dimas.Portal.SA
{
    public class FacebookRepository : IFacebookRepository
    {
        private static string appId = "980237178670805";
        private static string appSecret = "7dcc521f65957c0e8702994c99fb9f67";

        private static string _token=string.Empty;
        private static object obj =new object();

        private static string baseUrl = "https://graph.facebook.com/v2.2/";

        static string token 
        {
            get
            {
                if (string.IsNullOrEmpty(_token))
                {
                    _token = getToken();
                }
                return _token;
            }
        }


        private dynamic Proxy(string url,string UserToken="")
        {
            if (string.IsNullOrEmpty(UserToken))
            {
                UserToken = token;
            }
            var adjustmentUrl = url + "&access_token=" + UserToken;
            lock (obj)
            {
                using (WebClient client = new WebClient())
                {
                    string data = client.DownloadString(adjustmentUrl);
                    dynamic dynamicData = JObject.Parse(data);
                    if (dynamicData.error != null)
                    {
                        if (dynamicData.code == 190)
                        {
                            _token = string.Empty;
                        }
                        adjustmentUrl = url + "&access_token=" + token;
                        data = client.DownloadString(adjustmentUrl);
                        dynamicData = JObject.Parse(data);
                    }
                    return dynamicData;
                }
            }
        }

        static private string getToken()
        {
            var address = baseUrl+"/oauth/access_token?client_id=" + appId + "&client_secret=" + appSecret + "&grant_type=client_credentials"; 
            WebClient client = new WebClient();
            string token = client.DownloadString(address);
            return token;
        }

        public UserInfo GetDataByToken(string token)
        {
            var url = baseUrl + "me/?format=json&method=get&pretty=0&suppress_http_code=1";
            var data = Proxy(url, token);
            var retVal = new UserInfo()
            {
                providerName = "facebook",
                providerID = data.id,
                UserName = data.name,
                Email = data.email
            };
            return retVal;
        }

        public UserInfo getMyData(string userId)
        {
            var url = baseUrl + userId + "/?format=json&method=get&pretty=0&suppress_http_code=1";
            var data = Proxy(url);
            var retVal = new UserInfo() { 
                providerName="facebook",
                providerID=data.id,
                UserName = data.name,
                Email= data.email
            };
            return retVal;
        }

        public int postInvitation(PostInvitationInfo postInfo)
        {
            var fb = new FacebookClient(postInfo.access_token);
            var parameters = new Dictionary<string, object>();
            parameters.Add("message", postInfo.message);
            parameters.Add("link", postInfo.link);
            parameters.Add("place", postInfo.place);
            parameters.Add("tags", postInfo.tags);
            var res=fb.Post("me/feed", parameters);
            return 1;
        }
    }
}
