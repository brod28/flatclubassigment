﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Common.Utils;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;
using Dimas.Framework.Interface.Service;

namespace Dimas.Portal.SA
{
    public class LateBindingSampleRepository : ILateBindingSampleRepository
    {
        public SampleInfo GetById(int id)
        {
            var url = System.Configuration.ConfigurationManager.AppSettings["SampleServiceUrl"];
            BasicHttpBinding binding = new BasicHttpBinding();
            EndpointAddress address = new EndpointAddress(url);
            using (ChannelFactory<ISampleService> factory = new ChannelFactory<ISampleService>(binding, address))
            {

                ISampleService channel = factory.CreateChannel();
                var request = new RequestSampleService() { IdToSearch = id };

                var response = channel.GetById(request);
                var result = Mapper.Map<ResponseSampleService, SampleInfo>(response);
                return result;
            }

            

        }
    }
}
