﻿using System;
using Dimas.Portal.Tests.App_Start;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dimas.Portal.Tests.Infrastraction
{
    public class BaseUnitTest
    {
        protected BaseUnitTest()
        {
            StructuremapMvc.Start();
        }
    }
}
