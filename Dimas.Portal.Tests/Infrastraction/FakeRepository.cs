﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;
using Dimas.Portal.Tests.Infrastraction;

namespace Dimas.Portal.Tests
{
    public class FakeRepository<T> : IRepository<T> where T : BaseTemplate
    {

        public FakeRepository()
        {



            DeserializationObjectHelper<T> data;
            // Construct an instance of the XmlSerializer with the type
            // of object that is being deserialized.
            XmlSerializer mySerializer =
            new XmlSerializer(typeof(DeserializationObjectHelper<T>));
            // To read the file, create a FileStream.
            FileStream myFileStream =
            new FileStream("../../Data/" + typeof(T).Name + ".xml", FileMode.Open);
            // Call the Deserialize method and cast to the object type.
            data = (DeserializationObjectHelper<T>)
            mySerializer.Deserialize(myFileStream);
            myFileStream.Close();
            
            _items = new FakeDbSet<T>(data.List.ToArray());
        }
        
        


        private IDbSet<T> _items;

        public IDbSet<T> Items
        {
            get 
            { 
                return _items; 
            }
        }



        public int SaveChanges()
        {
            return 1;
        }

        public void Dispose()
        {
        }
    }
}
