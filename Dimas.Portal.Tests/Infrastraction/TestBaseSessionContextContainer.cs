﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dimas.Framework.Interface;

namespace Dimas.Portal.Tests.Infrastraction
{
    class TestBaseSessionContextContainer : IBaseSessionContextContainer
    {
        private string _userName="dima";

        public string userName
        {
            get 
            { 
                return _userName; 
            }
            set 
            {
                _userName = value; 
            }
        }

        private Guid _SessionTransactionId;
        public Guid SessionTransactionId
        {
            get
            {
                return _SessionTransactionId;
            }
            set
            {
                _SessionTransactionId = value;
            }
        }
    }
}
