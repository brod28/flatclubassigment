﻿using System;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface;
using Dimas.Portal.BL;
using Dimas.Portal.Tests.Infrastraction;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;

namespace Dimas.Portal.Tests
{
    [TestClass]
    public class StoryProviderTest : BaseUnitTest
    {
        [TestMethod]
        public void CreateStorySuccess()
        {
            var storyProvider = ObjectFactory.GetInstance<StoryProvider>();
            var entity=new StoryInfo(){
                Id=5,
                Content="A",
                Description="A",
                GroupRefId=1,
                UserRefId=3,
                Title="A",
                PostedOn=new DateTime(2015,1,1)
            };
            var result = storyProvider.CreateStory(entity);
            Assert.AreEqual(result, entity);
        }

        [TestMethod]
        public void GetStoriesByUserIdSuccess()
        {
            var storyProvider = ObjectFactory.GetInstance<StoryProvider>();
            var result = storyProvider.GetStoriesByUserId(1);
            Assert.AreEqual(result.Count, 4);
        }

        [TestMethod]
        public void GroupDetailsSuccess()
        {
            var storyProvider = ObjectFactory.GetInstance<StoryProvider>();
            var result = storyProvider.GroupDetails();
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Group.Name, "group1");
            Assert.AreEqual(result[0].SumOfStories, 3);
            Assert.AreEqual(result[0].SumOfUser, 1);
        }


        [TestMethod]
        public void GetGroupsSuccess()
        {
            var storyProvider = ObjectFactory.GetInstance<StoryProvider>();
            var result = storyProvider.GetGroups();
            Assert.AreEqual(result.Count, 3);
        }
    }
}
