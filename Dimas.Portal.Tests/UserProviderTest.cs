﻿using System;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface;
using Dimas.Portal.BL;
using Dimas.Portal.Tests.Infrastraction;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;

namespace Dimas.Portal.Tests
{
    [TestClass]
    public class UserProviderTest : BaseUnitTest
    {
        [TestMethod]
        public void GetUserByNameSuccess()
        {
            var UserProvider = ObjectFactory.GetInstance<UserProvider>();
            var result =UserProvider.GetUserByName("dima");
            Assert.AreEqual(result.Name, "dima");
            Assert.AreEqual(result.Id, 1);
        }

        [TestMethod]
        public void GetUserByNameFail()
        {
            var UserProvider = ObjectFactory.GetInstance<UserProvider>();
            var result = UserProvider.GetUserByName("dima2");
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void CreateUserSuccess()
        {
            var UserProvider = ObjectFactory.GetInstance<UserProvider>();
            var entity = new UserInfo() {
                Name = "dima2"
            };
            var result = UserProvider.CreateUser(entity);
            Assert.AreEqual(result.Name, entity.Name);
            Assert.AreEqual(result.Id, entity.Id);
            result = UserProvider.GetUserByName(entity.Name);
            Assert.AreEqual(result.Name, entity.Name);
            Assert.AreEqual(result.Id, entity.Id);
        }

    }
}
