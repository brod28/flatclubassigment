﻿using System.Web;
using System.Web.Mvc;
using Dimas.Portal.Filters;

namespace Dimas.Portal
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TracingFilter());
           // filters.Add(new AuthorizeAttributeFilter());
            filters.Add(new CultureFilter());
//            filters.Add(new HandleErrorAttribute());
        }
    }
}