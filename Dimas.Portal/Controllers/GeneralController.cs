﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Dimas.Framework.Entity;
using Dimas.Framework.Interface.Repostory;
using Dimas.Portal.BL;
using Dimas.Portal.DAL;
using Dimas.Portal.Models;

namespace Dimas.Portal.Controllers
{
    public class GeneralController : Controller
    {

        const string cookieName = "UserId";
        HttpCookie _myCookie = new HttpCookie(cookieName);

        //
        // GET: /Group/
        public ActionResult GetGroups()
        {
            var storyProvider = StructureMap.ObjectFactory.GetInstance<StoryProvider>();
            var retVal = (from i in storyProvider.GetGroups()
                          select new { Id = i.Id, Name = i.Name })
                           .ToList();
            

            return Json(retVal, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 120)]
        public ActionResult GroupDetails()
        {
            var storyProvider = StructureMap.ObjectFactory.GetInstance<StoryProvider>();
            var retVal = storyProvider.GroupDetails();
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateStory(StoryModel model)
        {

            var storyProvider = StructureMap.ObjectFactory.GetInstance<StoryProvider>();
            var entity = new StoryInfo()
            {
                Content = model.Content,
                Description = model.Description,
                Title = model.Title,
                GroupRefId = model.GroupId, 
                UserRefId = int.Parse(HttpContext.Request.Cookies[cookieName].Value),
                PostedOn = DateTime.UtcNow
            };
            storyProvider.CreateStory(entity);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login(string username)
        {

            var userProvider = StructureMap.ObjectFactory.GetInstance<UserProvider>();
            var User = userProvider.CreateUserIfNotExist(username);
            _myCookie.Value = User.Id.ToString();
            _myCookie.Expires = DateTime.Now.AddDays(365);
            HttpContext.Response.SetCookie(_myCookie);

            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MyStories()
        {
            int userId=int.Parse(HttpContext.Request.Cookies[cookieName].Value);
            var storyProvider = StructureMap.ObjectFactory.GetInstance<StoryProvider>();
            var retVal = storyProvider.GetStoriesByUserId(userId);
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }

    }
}
