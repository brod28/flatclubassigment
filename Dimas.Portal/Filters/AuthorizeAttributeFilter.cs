﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;
using StructureMap;
namespace Dimas.Portal.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthorizeAttributeFilter : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            context.Result = new RedirectToRouteResult(
                    new System.Web.Routing.RouteValueDictionary
                        {
                                { "culture",HttpContext.Current.Request.RequestContext.RouteData.Values["culture"] },
                                { "controller", "Account" },
                                { "action", "Login" },
                                { "ReturnUrl", context.HttpContext.Request.RawUrl }
                        });
        }
    }
}