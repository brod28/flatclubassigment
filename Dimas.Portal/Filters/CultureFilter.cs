﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;


using System.Globalization;
using System.Threading;

namespace Dimas.Portal.Filters
{
    public class CultureFilter:IActionFilter
    {


        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string langName = "en-US";
            if (filterContext.RouteData.Values["culture"] != null)
            {
                langName = filterContext.RouteData.Values["culture"].ToString();
            }
            CultureInfo ci = new CultureInfo(langName);
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);

        }



        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
    }
}