﻿using System.Web.Mvc;
using System.Text;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Portal.Filters
{
    public class TracingFilter : IActionFilter
    {
        private readonly ILoggerService _logger;

        public TracingFilter()
        {
            _logger = ObjectFactory.GetInstance<ILoggerService>();
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Request url {0}", filterContext.HttpContext.Request.Url.ToString());
            _logger.Info(sb.ToString());
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }
    }
}