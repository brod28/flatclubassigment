﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Portal
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            ILogConfig logConfig = ObjectFactory.GetInstance<ILogConfig>();
            logConfig.Start();

        }
        protected void Application_Error(object sender, EventArgs e)
        {
            ILoggerService _logger = ObjectFactory.GetInstance<ILoggerService>();

            //bool isUnexpectedException = true;
            HttpContext context = ((HttpApplication)sender).Context;

            Exception ex = context.Server.GetLastError();
            _logger.Error(ex);
            
            var _sessionContextContainer = ObjectFactory.GetInstance<IBaseSessionContextContainer>();
          //  context.Response.Redirect("/GeneralError.html?" + _sessionContextContainer.SessionTransactionId.ToString());
        }
    }
}