﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Portal.Logic
{
    public class BaseController : Controller
    {
        protected ILoggerService _logger = ObjectFactory.GetInstance<ILoggerService>();
        protected IBaseSessionContextContainer _sessionContextContainer = ObjectFactory.GetInstance<IBaseSessionContextContainer>();
    }
}