﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dimas.Framework.Interface;
using StructureMap;

namespace Dimas.Portal.App_Start
{
    public class SessionContextContainer : IBaseSessionContextContainer
    {

        public string userName
        {
            get
            {
                string name = string.Empty;
                if (HttpContext.Current.User != null)
                {
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        name = HttpContext.Current.User.Identity.Name;
                    }
                }
                return name;
            }
        }


        public Guid SessionTransactionId
        {
            get
            { 
                if(HttpContext.Current.Items["SessionTransactionId"]==null)
                {
                    HttpContext.Current.Items["SessionTransactionId"] = Guid.NewGuid();
                }
                return (Guid)HttpContext.Current.Items["SessionTransactionId"];
            }
        }
    }
}