﻿var LoginController = function ($scope, $http, $location) {
    $scope.formdirty = false;
    $scope.Login = function () {
        if ($scope.myForm.$valid) {
            $http.get('/en-us/general/Login?username=' + $scope.username).
          success(function (data, status, headers, config) {
              if (data.result == true) {
                  $location.path('/MyStories/');
              }
              else {
                  alert("error");
              }

          }).
          error(function (data, status, headers, config) {
              alert("error");
          });
        }
        else {
            $scope.formdirty = true;
        }
    }
}