﻿var MyStoriesController = function ($scope, $http, $location) {
    $scope.noItems = false;
    $scope.itemsPerIteration = 5;
    $scope.currentPageAmount = 0;
    $scope.isShowMoreButton = false;
    $scope.currentstories = [];
    if (!IsLogin()) {
        alert("to watch this page you have to be logged in");
        $location.path('/Login/');
    }
    else {
        $http.get('/en-us/General/MyStories').
             success(function (data, status, headers, config) {
                 $scope.mystories = data;
                 $scope.isShowMoreButton = true;
                 if ($scope.mystories.length == 0) {
                     $scope.noItems = true;
                 }
                 $scope.Add();
             }).
             error(function (data, status, headers, config) {
                 alert("error");
             });
    }

    $scope.Add = function () {
        var toAdd = Enumerable
            .From($scope.mystories)
            .Select(function (x) {
                return x;
            })
            .Skip($scope.currentPageAmount)
            .Take($scope.itemsPerIteration)
            .ToArray();


        for (i = 0; i < toAdd.length; i++)
        {
            $scope.currentstories.push(toAdd[i]);
        }
        if ($scope.currentstories.length >= $scope.mystories.length) {
            $scope.isShowMoreButton = false;
        }
        $scope.currentPageAmount = $scope.currentPageAmount + $scope.itemsPerIteration;
    }

    $scope.UnderConstructor = function () {
        alert("Sorry, this button is under construction");
    }

    $scope.CreateNewStory = function () {
        $location.path('/NewStory/');
    }
}