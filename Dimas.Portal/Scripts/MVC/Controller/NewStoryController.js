﻿var NewStoryController = function ($scope, $http, $location) {
    if (!IsLogin()) {
        alert("to watch this page you have to be logged in");
        $location.path('/Login/');
    }
    else {
        $scope.formdirty = false;
        $http.get('/en-us/General/GetGroups').
             success(function (data, status, headers, config) {
                 $scope.groups = data;
             }).
             error(function (data, status, headers, config) {
                 alert("error");
             });

        $scope.CreateNewStory = function () {
            if ($scope.myForm.$valid) {
                $http.post('/en-us/General/CreateStory', { Title: $scope.Title, Description: $scope.Description, Content: $scope.Content, GroupId: $scope.GroupId }).
                 success(function (data, status, headers, config) {
                     $location.path('/MyStories/');
                 }).
                 error(function (data, status, headers, config) {
                     alert("error");
                 });
            }
            else {
                $scope.formdirty = true;
            }

        }
    }
}