﻿angular.module('SharedServices', [])
    .config(function ($httpProvider) {
        $httpProvider.responseInterceptors.push('myHttpInterceptor');
        var spinnerFunction = function (data, headersGetter) {
            // todo start the spinner here
            //alert('start spinner');
            $('#mydiv').show();
            return data;
        };
        $httpProvider.defaults.transformRequest.push(spinnerFunction);
    })
// register the interceptor as a service, intercepts ALL angular ajax http calls
    .factory('myHttpInterceptor', function ($q, $window) {
        return function (promise) {
            return promise.then(function (response) {
                // do something on success
                // todo hide the spinner
                //alert('stop spinner');
                $('#mydiv').hide();
                return response;

            }, function (response) {
                // do something on error
                // todo hide the spinner
                //alert('stop spinner');
                $('#mydiv').hide();
                return $q.reject(response);
            });
        };
    });


var app = angular.module('DimasApp', ['ngRoute', 'ngSanitize', 'SharedServices']);

//, 'stringFormatterModule', 'ui.bootstrap', 'ngSanitize', 'kendo.directives', 'ngDialog', 'ui.bootstrap.collapse', 'piwik', 'duScroll', 'ng-context-menu']);
//IoC init
var _IoC = new IoC();
_IoC.init();


app.run(function ($rootScope) {
});

// configure our routes
app.config(function ($routeProvider) {
    $routeProvider
        .when('/Login', {
            templateUrl: domain + '/pages/Login.html',
            controller: 'LoginController'
        })
        .when('/MyStories', {
            templateUrl: domain + '/pages/MyStories.html',
            controller: 'MyStoriesController'
        })
        .when('/Group', {
            templateUrl: domain + '/pages/Group.html',
            controller: 'GroupController'
        })
        .when('/NewStory', {
            templateUrl: domain + '/pages/NewStory.html',
            controller: 'NewStoryController'
        })
    

    
        .otherwise({ redirectTo: '/Login' });
}).config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
      // Allow same origin resource loads.
      'self',
      // Allow loading from our assets domain.  Notice the difference between * and **.
      'http://**'
    ]);

});;

var IsLogin = function () {
    var getCookie = function (name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
                end = dc.length;
            }
        }
        return unescape(dc.substring(begin + prefix.length, end));
    }

    return getCookie("UserId") != null;

}