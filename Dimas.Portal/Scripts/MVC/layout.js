﻿function RefreshLayout() {
    var isSideBarMenu =  IsSideBarMenu();
    var isSaverSideBar = IsSaverSideBar();
    var sideBarHtml = "";

    if (isSideBarMenu) {
        $('#renderBody').attr("class", "span8 main-ness-body")
        $('#SideBar').attr("class", "span3 shadow main-sidebar-body")

        if (!isSaverSideBar) {
            sideBarHtml = RefreshSideBar();
            $("#defaultSideBar").html(sideBarHtml);
            $("#saverSideBar").hide();
            $("#defaultSideBar").show();
        }
        else {
            $("#saverSideBar").show();
            $("#defaultSideBar").hide();
        }

    }
    else {
        $('#renderBody').attr("class", "span12 login-view")
        $('#SideBar').attr("class", "notDisplay")
    }
}

var IsSideBarMenuCallback = function (response) {
    if (response.data.Data) {
        $('#renderBody').attr("class", "span8 main-ness-body")
        $('#SideBar').attr("class", "span3 shadow main-sidebar-body")
        IsSaverSideBar();
    }
    else {
        $('#renderBody').attr("class", "span12 login-view")
        $('#SideBar').attr("class", "notDisplay")
    }
}


var IsSaverSideBarCallback = function (response) {
    if (!response.data.Data) {
        RefreshSideBar();
    }
    else {
        $("#saverSideBar").show();
        $("#defaultSideBar").hide();
    }
}

var RefreshSideBarCallback = function (response) {
    $("#defaultSideBar").html(response.data.Data);
    $("#saverSideBar").hide();
    $("#defaultSideBar").show();
}

function IsSideBarMenu() {
    var ajaxHelper = new AjaxHelper();

    var jsonOptions = {
        data: {},
        url: context.param.url.IsSideBarMenu(),
        options: { type: "GET", dataType: "json", contentType: "application/json; charset=utf-8"}//, callback: IsSideBarMenuCallback, loader: {active:false} }
    };
    var retVal = ajaxHelper.getJson(jsonOptions);
    return retVal.data.Data;
    //return retVal;
}

function IsSaverSideBar() {
    var ajaxHelper = new AjaxHelper();

    var jsonOptions = {
        data: {},
        url: context.param.url.IsSaverSideBar(),
        options: { type: "GET", dataType: "json", contentType: "application/json; charset=utf-8"}//, callback: IsSaverSideBarCallback, loader: {active:false} }
    };
    var retVal = ajaxHelper.getJson(jsonOptions);
    return retVal.data.Data;
    //return retVal;
}

function RefreshSideBar() {
    var ajaxHelper = new AjaxHelper();

    var jsonOptions = {
        data: {},
        url: context.param.url.RefreshSideBar(),
        options: { type: "GET", dataType: "json", contentType: "application/json; charset=utf-8"}//, callback: RefreshSideBarCallback, loader: {active:false} }
    };
    var retVal = ajaxHelper.getJson(jsonOptions);
    return retVal.data.Data;
    //return retVal;
}