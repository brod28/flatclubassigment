// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using Dimas.Framework.Common.Base;
using Dimas.Framework.Common.Cache;
using Dimas.Framework.Common.Log;
using Dimas.Framework.Common.Utils;
using Dimas.Framework.Interface;
using Dimas.Framework.Interface.Repostory;
using Dimas.Service.DAL;
using StructureMap;
namespace Dimas.Service.DependencyResolution{
    public static class IoC {
        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });

                            x.For<IBaseSessionContextContainer>().Use<FakeBaseSessionContextContainer>();
                            x.For<ICacheManager>().Use<CacheManager>();
                            x.For<ILoggerService>().Use<Log4NetLoggerService>();
                            x.For<ILogConfig>().Use<Log4NetConfig>();
                            x.For<IConfiguration>().Use<WebConfiguration>();
                            x.For<IEntityFrameworkSampleRepository>().Use<EntityFrameworkSampleRepository>();
                            x.For<IADOSampleRepository>().Use<ADOSampleRepository>();
                        });
            return ObjectFactory.Container;
        }
    }
}